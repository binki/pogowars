extern "C" {
#include <math.h>
#include <SDL.h>
}

#include "circle.hxx"


circle::circle(double start_x, double start_y, double radius, double dispRadius, double max_speed){
	
	position_x = start_x;
	position_y = start_y;
	velocity_x = 0;
	velocity_y = 0;
	this->radius = radius;
	this->dispRadius = dispRadius;
	this->max_speed = max_speed;
}


double circle::vectorSpeed(){
	
	return sqrt (pow(velocity_x, 2) + pow(velocity_y,2)); 
}

double circle::vectorAngle(){
		double x = velocity_x;
		double y = velocity_y;
	if (fabs(x) < 0.0001)
		return y > 0 ? 90 : 270;
	else
		return fmod(180 * atanf(y/x) / M_PI + (x > 0 ? 0 : 180) + 360, 360);
}
	
circle::~circle() {
}
