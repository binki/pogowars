#ifndef _TESTSDL_CIRCLE_HXX
#define _TESTSDL_CIRCLE_HXX

extern "C" {
#include <SDL.h>
}

class circle {
public:
	double position_x;
	double position_y;
	double velocity_x;
	double velocity_y;
	double radius;
	double dispRadius;
	double max_speed;
	double angle;
	//Vector speed function inside circle class to make it easier to get the vector
	double vectorSpeed (void);
	double vectorAngle (void);
	SDL_Texture *tex;
	
	circle(double start_x, double start_y, double radius, double dispRadius, double max_speed);
	virtual ~circle();
};

#endif
