#ifndef _TESTSDL_ONE_PLAYER_GAME_MODE_HXX
#define _TESTSDL_ONE_PLAYER_GAME_MODE_HXX

extern "C" {
#include <SDL.h>
#include <math.h>
}

#include "camera.hxx"
#include "game_mode.hxx"
#include "sprite.hxx"
#include "textured_circle.hxx"

// This subclass of circle handles tracking, rendering, cleaning its own texture.



	

class one_player_game_mode : public game_mode {
public:
	one_player_game_mode(SDL_Renderer *ren);

	void set_map(const char *map);
	virtual bool processEvents(SDL_Event *event, int *current_game_mode);
	virtual void animate();
	virtual void render(SDL_Renderer *ren, TTF_Font *font);
	virtual ~one_player_game_mode();

private:
	void clear();
	camera my_camera; 
	textured_circle defaultPlayer, player;
	textured_circle defaultPlayerTwo, playerTwo;
	SDL_Texture *tex_fireAnimation;
	sprite fireSprite;
	double playerAcceleration;
	int score;
	SDL_Texture *tex_map;
	SDL_Texture *tex_foreground;
	SDL_Texture *tex_smoke_Room_Furnace;
	bool left, right, up, down;
	bool left2, right2, up2, down2;
	bool showScore;
};

#endif /* _TESTSDL_ONE_PLAYER_GAME_MODE_HXX */
