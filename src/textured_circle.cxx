extern "C" {
#include <math.h>
#include <SDL.h>
}

#include "textured_circle.hxx"
#include "main.hxx"

textured_circle::textured_circle(SDL_Renderer *ren, const char *texfile, double start_x, double start_y, double radius, double dispRadius, double max_speed)
: circle(start_x, start_y, radius, dispRadius, max_speed)
{
	this_that_should_kill_tex = this;
	tex = loadTexture(ren, texfile);
}

void textured_circle::render(SDL_Renderer *ren, camera *my_camera) {
	SDL_Rect dst;
	
	dst.x = position_x - dispRadius;
	dst.y = position_y - dispRadius;
	dst.w = dispRadius*2;
	dst.h = dispRadius*2;
	my_camera->transform(&dst);
	SDL_RenderCopyEx(ren, tex, NULL, &dst, /*angle*/ 0, NULL, SDL_FLIP_NONE);
}

textured_circle::~textured_circle() {
	// HACK: avoid double-free (the wrong way) by checking that we were actually the object that called loadTexture() using the stored this token which gets copied with the automatically-implemented shallow copy.
	if (this_that_should_kill_tex == (void*)this)
	/*{std::cerr << "destrolying thing " << ((size_t)tex) << " because I am " << ((size_t)(void*)this) << std::endl;*/
		SDL_DestroyTexture(tex);
		/*} else std::cerr << "not destryojing thing " << ((size_t)tex) << " because I am " << ((size_t)(void*)this) << " instead of " << ((size_t)this_that_should_kill_tex) << std::endl;*/
		//Nathan mumbo jumbo how he do that
}
