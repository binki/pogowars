#ifndef _TESTSDL_TEXTURED_CIRCLE_HXX
#define _TESTSDL_TEXTURED_CIRCLE_HXX

#include "circle.hxx"
#include "camera.hxx"

class textured_circle : public circle {
private:
	void *this_that_should_kill_tex; // HACK
	SDL_Texture *tex;
	
public:
	textured_circle(SDL_Renderer *ren, const char *texfile, double start_x, double start_y, double radius, double dispRadius, double max_speed);
	
	void render(SDL_Renderer *ren, camera *my_camera);
	
	virtual ~textured_circle();
};
#endif
